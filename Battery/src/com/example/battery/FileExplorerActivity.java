package com.example.battery;

import java.io.File;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;


//import ru.sitis.geoscamera.R;
//import ru.sitis.geoscamera.settings.SettingsConst;
//import ru.sitis.geoscamera.utils.DirsUtils;
import com.example.battery.R;
//import com.example.dialogfragments.DirsUtils;
//import ru.sitis.geoscamera.utils.FileExtensionsFilter;
import com.example.battery.FileExtensionsFilter;
import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.AdapterView.OnItemClickListener;
import android.widget.BaseAdapter;
import android.widget.Button;
import android.widget.ListView;
import android.widget.TextView;

public class FileExplorerActivity extends Activity {
	/*
	 * View
	 */
	// ������ �������� �����
	private Button mOkButton;
	// ������
	private ListView mListView;
	// ����������� ������� �������
	private TextView mTxtCurrentPath;

	private File mPath;
	// ������� ������
	private int mLevel;
	// private boolean mIsShowDirectory;

	private FileListAdapter mAdapter;
	// private onDirectoryOrFileSelected mListener;

	// ������ �������������� ���� ������
	private ArrayList<String> mountedCards;

	private MODE mModeExplorer;
	private FileExtensionsFilter mFileFilter;

	public enum MODE {
		DIRECTORIES, ALL_FILES, SAME_FILES
	}

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.fragment_file_explorer);

		Intent intent = getIntent();
		boolean isShowDirectory = intent.getBooleanExtra(SettingsConst.KEY_SHOW_DIRECTORIES, false);
		String[] extentionArray = intent.getStringArrayExtra(SettingsConst.KEY_EXTENTION_ARRAY);
		if (extentionArray != null) {
			mFileFilter = new FileExtensionsFilter(extentionArray);
			if (isShowDirectory) {
				mFileFilter.setDirectoryEnabled(isShowDirectory);
			}
		}

		int item = intent.getIntExtra(SettingsConst.KEY_MODE_EXPLORER, MODE.DIRECTORIES.ordinal());
		mModeExplorer = MODE.values()[item];

		// ������� ������������ ����� GeoS
		String curPath = intent.getStringExtra(SettingsConst.KEY_CURRENT_DIRECTORY);
		
		if (curPath == null) return;

		mTxtCurrentPath = (TextView) findViewById(R.id.tv_current_path);
		mOkButton = (Button) findViewById(R.id.btn_ok);
		mListView = (ListView) findViewById(R.id.listView_files);

		if (mModeExplorer != MODE.DIRECTORIES) {
			mOkButton.setVisibility(View.GONE);
		} else {
			mOkButton.setVisibility(View.VISIBLE);
		}

		mTxtCurrentPath.setText(curPath);
		mountedCards = DirsUtils.getStorages();
		mPath = new File(curPath);
		mLevel = getLeverDirectory(curPath);

		mAdapter = new FileListAdapter();
		mListView.setAdapter(mAdapter);
		mListView.setOnItemClickListener(new FileItemClickListener());

		mOkButton.setOnClickListener(new PositiveButtonListener());
	}

	// public static FileExplorerFragment newInstance(String path) {
	// FileExplorerFragment fragment = new FileExplorerFragment();
	// Bundle arguments = new Bundle();
	// arguments.putString(SettingsConst.KEY_CURRENT_DIRECTORY, path);
	//
	// fragment.setArguments(arguments);
	// return fragment;
	// }

	// public void setListener(onDirectoryOrFileSelected listener) {
	// mListener = listener;
	// }

	// public void setModeExplorer(MODE mode, FileExtensionsFilter fileFilter) {
	// mModeExplorer = mode;
	// mFileFilter = fileFilter;
	// }

	// public interface onDirectoryOrFileSelected {
	// public void onDirectoryOrFileChange(String path);
	// }

	@Override
	public void onBackPressed() {
		// TODO Auto-generated method stub
		super.onBackPressed();
		setResult(RESULT_CANCELED);
	}

	/**
	 * �������� ������� ����� ������������ �����
	 */
	private int getLeverDirectory(String path) {
		int level = 0;
		if (!mountedCards.contains(path)) {
			File temp = new File(path);
			while (temp.exists()
					&& !mountedCards.contains(temp.getAbsolutePath())) {
				temp = temp.getParentFile();
				level++;

			}
		}
		return level;

	}

	private class FileListAdapter extends BaseAdapter {

		private ArrayList<String> mmListItems;
		private LayoutInflater mmInflater;

		public FileListAdapter() {
			String[] list = null;
			switch (mModeExplorer) {

			case DIRECTORIES:
				list = DirsUtils.getDirectoryList(mPath);
				mmListItems = new ArrayList<String>(Arrays.asList(list));
				break;

			case ALL_FILES:
				list = mPath.list();
				mmListItems = new ArrayList<String>(Arrays.asList(list));
				break;

			case SAME_FILES:
				if (mFileFilter != null) {
					File[] listFiles = mPath.listFiles(mFileFilter);
					if (listFiles == null)
						return;
					mmListItems = new ArrayList<String>();
					for (File temp : listFiles) {
						mmListItems.add(temp.getName());
					}
				}
			}

			Collections.sort(mmListItems);

			if (mLevel >= 0) {
				mmListItems.add(0, getResources().getString(R.string.up));
			}
			mmInflater = (LayoutInflater) getSystemService(LAYOUT_INFLATER_SERVICE);

		}

		@Override
		public int getCount() {
			return mmListItems.size();
		}

		@Override
		public String getItem(int position) {
			return mmListItems.get(position);
		}

		@Override
		public long getItemId(int position) {
			return position;
		}

		@Override
		public View getView(int position, View convertView, ViewGroup parent) {
			View view = mmInflater.inflate(R.layout.list_item_file_explorer,
					null);
			TextView nameFile = (TextView) view.findViewById(R.id.tv_file_name);
			nameFile.setText(getItem(position));
			return view;
		}

		/**
		 * ���������� ��������
		 */
		public void refresh() {
			String[] list = null;
			switch (mModeExplorer) {

			case DIRECTORIES:
				list = DirsUtils.getDirectoryList(mPath);
				mmListItems = new ArrayList<String>(Arrays.asList(list));
				break;

			case ALL_FILES:
				list = mPath.list();
				mmListItems = new ArrayList<String>(Arrays.asList(list));
				break;

			case SAME_FILES:
				if (mFileFilter != null) {
					File[] listFiles = mPath.listFiles(mFileFilter);
					if (listFiles == null)
						return;
					mmListItems = new ArrayList<String>();
					for (File temp : listFiles) {
						mmListItems.add(temp.getName());
					}
				}
			}
			Collections.sort(mmListItems);
			if (mLevel >= 0) {
				mmListItems.add(0, getResources().getString(R.string.up));
			}
			notifyDataSetChanged();
		}

	}

	/**
	 * ��������� ��������� ����� �������, ����������� �� �������� �������
	 * 
	 * @author Zwerewa
	 * 
	 */
	private class FileItemClickListener implements OnItemClickListener {

		@Override
		public void onItemClick(AdapterView<?> arg0, View arg1, int pos,
				long arg3) {

			if (pos == 0 && mLevel > 0) {

				mPath = mPath.getParentFile();
				mLevel--;
				mAdapter.refresh();
			} else if (pos == 0 && mLevel == 0) {
				mAdapter.mmListItems = new ArrayList<String>(mountedCards);
				mLevel--;
				mAdapter.notifyDataSetChanged();
			} else {
				String name = mAdapter.getItem(pos);
				
				if (mLevel < 0) {
					mPath = new File(name);
				} else {
					mPath = new File(mPath, name);
				}
				
				switch (mModeExplorer) {
				case ALL_FILES:
				case SAME_FILES:
					if (!mPath.isDirectory()) {
						
						Intent intent = new Intent();
						intent.putExtra(SettingsConst.KEY_RESULT_PATH, mPath.getAbsolutePath());
						setResult(RESULT_OK, intent);
						finish();
						return;
					}
					break;
				
				}
				
				mLevel++;
				mAdapter.refresh();
			}

			mTxtCurrentPath.setText(mPath.getAbsolutePath());

		}

	}

	/**
	 * ��������� ������� ������ ��
	 */
	private class PositiveButtonListener implements OnClickListener {

		@Override
		public void onClick(View v) {

			String path = mPath.getAbsolutePath();
			if (path.length() > 0) {

				if (path.endsWith(File.separator)) {
					path = path.substring(0, path.length() - 1);
				}
				
				Intent intent = new Intent();
				intent.putExtra(SettingsConst.KEY_RESULT_PATH, path);
				setResult(RESULT_OK, intent);
				finish();

			}

		}
	}

}

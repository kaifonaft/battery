package com.example.battery;

import java.io.BufferedWriter;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileWriter;
import java.util.Date;

import android.app.Service;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.content.SharedPreferences;
import android.content.SharedPreferences.Editor;
import android.os.BatteryManager;
import android.os.Build;
import android.os.Handler;
import android.os.IBinder;
import android.os.Environment;
import android.util.Log;
import org.json.JSONObject;

import java.text.SimpleDateFormat;

public class LogBatteryService extends Service {
	private static boolean serviceIsRun;
    private static int query_interval;
    private static String comment;
    public int battery_level;
    public int battery_scale;
    public int temperature;
    public int voltage;
    private static String logFileName = "";
    private static File logDirectory;
    private static final String prefix = "log", suffix = ".json";
    private BroadcastReceiver batteryChangeReciever;
    
    Handler h;
    Runnable r = new Runnable() {
		
		@Override
		public void run() {
			// TODO Auto-generated method stub
			Log.d("run", "query");
			logBattery();
			if(query_interval==0)
				query_interval = 1;
			h.postDelayed(r, query_interval*1000);
		}
	};

	@Override
	public IBinder onBind(Intent arg0) {
		Log.d("service","bind");
		// TODO Auto-generated method stub
		return null;
	}
	public void onCreate(){
		super.onCreate();
        h = new Handler();
        Log.d("service","create service");
		h.post(r);
	}
	public void onDestroy(){
		super.onDestroy();
		h.removeCallbacks(r);
		serviceIsRun = false;
		unregisterReceiver(batteryChangeReciever);
		Log.d("service","destroy service");
	}
	public int onStartCommand(Intent intent, int flags, int startId){
		serviceIsRun = true;
//		logDirectory = Environment.getExternalStorageDirectory();
		logDirectory = new File(intent.getStringExtra("log_directory"));
        query_interval = intent.getIntExtra("query_interval",1);
        comment = intent.getStringExtra("comment");
        
        String commentString = intent.getStringExtra("comment");
        IntentFilter ifilterBC = new IntentFilter(Intent.ACTION_BATTERY_CHANGED);
        Intent batteryStatus = registerReceiver(null, ifilterBC);
        this.battery_level = batteryStatus.getIntExtra(BatteryManager.EXTRA_LEVEL, -1);
        this.battery_scale = batteryStatus.getIntExtra(BatteryManager.EXTRA_SCALE, -1);
        this.temperature = batteryStatus.getIntExtra(BatteryManager.EXTRA_TEMPERATURE, -1);
        this.voltage = batteryStatus.getIntExtra(BatteryManager.EXTRA_VOLTAGE, -1);
        Log.d("service", "service is run");

        try{
        	JSONObject config = new JSONObject(), comment = new JSONObject(), device_info = new JSONObject();
	        config.put("query_interval", query_interval);
	        comment.put("comment", commentString);
	        device_info.put("BRAND", Build.BRAND);
	        device_info.put("MODEL", Build.MODEL);
	        device_info.put("VERSION", Build.VERSION.RELEASE);

	        device_info.put("MODEL", Build.PRODUCT);
	        device_info.put("MODEL", Build.BOARD);
	        device_info.put("MODEL", Build.DEVICE);
	        device_info.put("MODEL", Build.DISPLAY);
	        device_info.put("MODEL", Build.HARDWARE);
	        device_info.put("MODEL", Build.ID);
	        device_info.put("MODEL", Build.CPU_ABI);
	        device_info.put("MODEL", Build.CPU_ABI2);
	        device_info.put("MODEL", Build.HOST);
	        device_info.put("MODEL", Build.FINGERPRINT);
	        device_info.put("MODEL", Build.BOOTLOADER);

	        Log.d("device info", Build.BRAND);
	        Log.d("device info", Build.MODEL);
	        Log.d("device info", Build.VERSION.RELEASE);
//	        Log.d("device info", Build.PRODUCT);
//	        Log.d("device info", Build.BOARD);
//	        Log.d("device info", Build.DEVICE);
//	        Log.d("device info", Build.DISPLAY);
//	        Log.d("device info", Build.HARDWARE);
//	        Log.d("device info", Build.ID);
//	        Log.d("device info", Build.TYPE);
//	        Log.d("device info", Build.CPU_ABI);
//	        Log.d("device info", Build.CPU_ABI2);
//	        Log.d("device info", Build.HOST);
//	        Log.d("device info", Build.FINGERPRINT);
//	        Log.d("device info", Build.BOOTLOADER);
	        
	        logFileName = defineLogName();
			
//			stop by power or not
			SharedPreferences sPref = getSharedPreferences(ConfigDefault.PREFERENCES_NAME, MODE_PRIVATE);
			boolean stop_by_power = sPref.getBoolean(ConfigKey.stop_by_power, ConfigDefault.DEFAULT_STOP_BY_POWER);
			Log.d("stop by power", String.valueOf(stop_by_power));
	        if(stop_by_power){
		        String continue_log_name = sPref.getString(ConfigKey.continue_log_name, "");
		        if(continue_log_name == "")
		        	throw new Exception("wrong continue_log_name");
	        	logFileName = continue_log_name;
				Log.d("continue_log_name", continue_log_name);
	        }else{
	        	Editor ed = sPref.edit();
	        	ed.putBoolean(ConfigKey.stop_by_power, true);
	        	ed.putString(ConfigKey.continue_log_name, logFileName);
	        	ed.commit();
		        File f = new File(logDirectory, logFileName);
				BufferedWriter jsonFile = new BufferedWriter(new FileWriter(f,true));
				jsonFile.write(device_info.toString());
				jsonFile.write(","+config.toString());
				jsonFile.write(","+comment.toString());
				jsonFile.flush();
				jsonFile.close();
	        }
	        
	        Intent logIntent = new Intent("LogServiceStart");
	        logIntent.putExtra("log_name", logFileName);
	        sendBroadcast(logIntent);
        }catch(Exception e){
        	e.printStackTrace();
        }
		
        batteryChangeReciever = new BroadcastReceiver(){
        	public void onReceive(Context context, Intent intent){
        		LogBatteryService.this.battery_level = intent.getIntExtra(BatteryManager.EXTRA_LEVEL, -1);
        		LogBatteryService.this.temperature = intent.getIntExtra(BatteryManager.EXTRA_TEMPERATURE, -1);
        		LogBatteryService.this.voltage = intent.getIntExtra(BatteryManager.EXTRA_VOLTAGE, -1);
        		Log.d("battery", "changed");
        	}
        };
        registerReceiver(batteryChangeReciever, ifilterBC);
 		return super.onStartCommand(intent, flags, startId);
	}
	
	// ��������� ���������� ������� � ���������� � � ���
	void logBattery(){
		try{
	        int level = this.battery_level;
	        int scale = this.battery_scale;
	        int battery_level = level*100/scale;
	        
	        int temp = this.temperature;
	        int voltage = this.voltage;

	        Date now = new Date();
			SimpleDateFormat formatter = new SimpleDateFormat("yyyy.MM.dd hh:mm:ss");
			String time = formatter.format(now);


			String toLog = "time: \""+time+"\" battery level: " + String.valueOf(battery_level) + " temperature: "+String.valueOf(temp) + " voltage: " + String.valueOf(voltage)+"\n";
			Log.d("res", toLog);
			
		    if (!Environment.getExternalStorageState().equals( Environment.MEDIA_MOUNTED )) {
				Log.d("enivronment", "SD-����� �� ��������: " + Environment.getExternalStorageState());
				return;
	        }
			File f = new File(logDirectory, logFileName);
			
			BufferedWriter jsonFile = new BufferedWriter(new FileWriter(f,true));

//			generate code:
//			Calendar c = Calendar.getInstance(), lastdate = Calendar.getInstance();
//			lastdate.add(Calendar.WEEK_OF_MONTH, 2);
//			int it = 0;
//			while(c.compareTo(lastdate) < 0){
//				time = formatter.format(c.getTime());
//				battery_level = (int)(50 + 25 * Math.sin(it * 1.0 / 100));
//				temp = (int)(300 + 50 * Math.sin(it * 1.0 / 100));
//				voltage = (int)(4300 + 100 * Math.sin(it * 1.0 / 100));
				
				JSONObject strJSON = new JSONObject();
				strJSON.put("bl", battery_level);
				strJSON.put("t", temp);
				strJSON.put("v", voltage);
				strJSON.put("time", time);
				
				if(f.exists() && f.length()!=0){
					jsonFile.write(",");
				}
				jsonFile.write(strJSON.toString());
				
//				c.add(Calendar.MINUTE, 5);
//				it++;
//			}
//			end generate code
			
			jsonFile.flush();
			jsonFile.close();
		}catch (FileNotFoundException e) {
			e.printStackTrace();
		}catch(Exception e){
			e.printStackTrace();
		}
	}
//	����������� ����� ���������� �� ����� ����
	public static String defineLogName(){
		String logName = "";
		String numLogStr;
		int i=1;
		while(i < 1000000){
			if(i<10){
				numLogStr = "00"+String.valueOf(i);
			}else if(i<100){
				numLogStr = "0"+String.valueOf(i);
			}else{
				numLogStr = String.valueOf(i);
			}
			logName = prefix+numLogStr+suffix;
			File f = new File(logDirectory, logName);
			if(!f.exists())
				break;
			i++;
		}
		return logName; 
	}
	public static boolean getServiceIsRun(){
		return serviceIsRun;
	}
	public static int getQueryInterval(){
		return query_interval;
	} 
	public static String getLogFileNamePrefix(){
		return prefix;
	}
	public static String getLogFileNameSuffix(){
		return suffix;
	}
	public static String getLogFileName(){
		return logFileName;
	}
	public static String getComment(){
		return comment;
	}
}

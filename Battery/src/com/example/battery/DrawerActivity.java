package com.example.battery;

import android.support.v4.app.ActionBarDrawerToggle;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.content.res.Configuration;
import android.os.Bundle;
import android.support.v4.app.FragmentActivity;
import android.support.v4.widget.DrawerLayout;
import android.support.v4.widget.DrawerLayout.DrawerListener;
import android.util.Log;
import android.view.MenuItem;
import android.view.View;
import android.view.inputmethod.InputMethodManager;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.ListView;

public abstract class DrawerActivity extends FragmentActivity{
//	common Activity for drawer
	protected int currentTab;
	private DrawerLayout mDrawerLayout;
	private ListView mDrawerList;
	private CharSequence mDrawerTitle, mTitle;
    protected String mDrawerTitles[];
    private ActionBarDrawerToggle mDrawerToggle;

	public static final int CONFIG = 0;
	public static final int RESULT = 1;
	public static final int ABOUT = 2;

	protected void onCreate(Bundle savedInstanceState) {
	    super.onCreate(savedInstanceState);
	    setContentView(R.layout.activity_main);
	    
	    mDrawerTitle = mTitle = getTitle();
	    
	    mDrawerTitles = new String[]{"config","result", "about"};
	    mDrawerList = (ListView) findViewById(R.id.left_drawer);
	    mDrawerList.setAdapter(new ArrayAdapter<String>(this, android.R.layout.simple_list_item_1, mDrawerTitles));
	    mDrawerList.setOnItemClickListener(new DrawerItemClickListener());
	    mDrawerLayout = (DrawerLayout) findViewById(R.id.drawer_layout);
	    
	    

        mDrawerToggle = new ActionBarDrawerToggle(
                this,                  /* host Activity */
                mDrawerLayout,         /* DrawerLayout object */
                R.drawable.ic_drawer,  /* nav drawer icon to replace 'Up' caret */
                R.string.drawer_open,  /* "open drawer" description */
                R.string.drawer_close  /* "close drawer" description */
                ) {

            /** Called when a drawer has settled in a completely closed state. */
            public void onDrawerClosed(View view) {
                super.onDrawerClosed(view);
//                getActionBar().setTitle(mTitle);
            }

            /** Called when a drawer has settled in a completely open state. */
            public void onDrawerOpened(View drawerView) {
	            Log.d("drawer", "action bar drawer open");
				InputMethodManager inputMethodManager = (InputMethodManager)  DrawerActivity.this.getSystemService(Activity.INPUT_METHOD_SERVICE);
			    inputMethodManager.hideSoftInputFromWindow(DrawerActivity.this.getCurrentFocus().getWindowToken(), 0);
                super.onDrawerOpened(drawerView);
            }
        };

        // Set the drawer toggle as the DrawerListener
        mDrawerLayout.setDrawerListener(mDrawerToggle);
//	    mDrawerLayout.setDrawerListener(new myDrawerListener());
	    selectItem(CONFIG);
	    Log.d("drawer activity","oncreate()");

        getActionBar().setDisplayHomeAsUpEnabled(true);
        getActionBar().setHomeButtonEnabled(true);
 
	}
	abstract protected void selectItem(int tab);
	
    @Override
    protected void onPostCreate(Bundle savedInstanceState) {
        super.onPostCreate(savedInstanceState);
        // Sync the toggle state after onRestoreInstanceState has occurred.
        mDrawerToggle.syncState();
    }

    @Override
    public void onConfigurationChanged(Configuration newConfig) {
        super.onConfigurationChanged(newConfig);
        mDrawerToggle.onConfigurationChanged(newConfig);
    }

	
	private class DrawerItemClickListener implements ListView.OnItemClickListener {
	    @Override
	    public void onItemClick(AdapterView parent, View view, int position, long id) {
	        // Highlight the selected item, update the title, and close the drawer
	    	Log.d("drawer", String.valueOf(position));
	    	mDrawerList.setItemChecked(position, true);
	    	selectItem(position);
//	        setTitle(mDrawerMenu[position]);
	        mDrawerLayout.closeDrawer(mDrawerList);
	    }
	}
    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Pass the event to ActionBarDrawerToggle, if it returns
        // true, then it has handled the app icon touch event
        if (mDrawerToggle.onOptionsItemSelected(item)) {
          return true;
        }
        // Handle your other action bar items...

        return super.onOptionsItemSelected(item);
    }
/*
    public class myDrawerListener implements DrawerListener{
		@Override
		public void onDrawerClosed(View arg0) {
			// TODO Auto-generated method stub
		}
	
		@Override
		public void onDrawerOpened(View arg0) {
			Log.d("drawer", "my drawer listener opened");
			// TODO Auto-generated method stub
		}
	
		@Override
		public void onDrawerSlide(View arg0, float arg1) {
			// TODO Auto-generated method stub
		}
	
		@Override
		public void onDrawerStateChanged(int arg0) {
			// TODO Auto-generated method stub
		}
	}
*/
}

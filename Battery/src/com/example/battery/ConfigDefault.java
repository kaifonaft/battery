package com.example.battery;

import android.os.Environment;
/*
 * �������� �������� "�� ���������" (preferences)*/
public class ConfigDefault {
    public static final int DEFAULT_INTERVAL = 300;
    public static final String DEFAULT_LOG_DIR = Environment.getExternalStorageDirectory().getAbsolutePath();
    public static final String DEFAULT_COMMENT = "";
    public static final boolean DEFAULT_AUTORUN = true;
    public static final boolean DEFAULT_STOP_BY_POWER = false;
    public static final String DEFAULT_CONTINUE_LOG_NAME = null;
    public static final String PREFERENCES_NAME = "logService";
}

package com.example.battery;

import android.os.Bundle;
import android.os.Environment;
import android.support.v4.app.Fragment;
import android.text.Editable;
import android.text.TextWatcher;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import com.example.battery.FileExplorerActivity;
import com.example.battery.SettingsConst;
import com.example.battery.FileExplorerActivity.MODE;

import android.preference.PreferenceManager;
import android.app.Activity;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.content.SharedPreferences;
import android.content.SharedPreferences.Editor;
import android.graphics.Color;
import android.util.Log;
import android.view.View.OnClickListener;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.EditText;
import android.widget.TextView;
public class ConfigFragment extends Fragment implements OnClickListener{
	/*
	 * �������� ��� ����������� ��������
	 * */
    private boolean countIsRun = false;
//  ���� ��������� ������
    private EditText intervalEditText;
//  ���� �����������
    private EditText commentEditText;
    private TextView log_dirTextView;
//	��� �������� ����
    private TextView logNameTextView;
    
    private Button startButton;
//    private Button change_dirButton;
//  ����������
    private CheckBox autorunCheckBox;
    private BroadcastReceiver serviceBroadcastReceiver;
    
    public final int REQUEST_LOG_DIRECTORY = 1;

    private SharedPreferences sPref;
    
	@Override
	public View onCreateView(LayoutInflater inflater, ViewGroup container,
			Bundle savedInstanceState) {
		// TODO Auto-generated method stub
        super.onCreate(savedInstanceState);
		View view = inflater.inflate(R.layout.config, container, false);

        startButton = (Button) view.findViewById(R.id.startButtonId);

		intervalEditText = (EditText) view.findViewById(R.id.intervalQueryTextEdit);
		commentEditText = (EditText) view.findViewById(R.id.commentEditText);
		logNameTextView = (TextView) view.findViewById(R.id.log_nameTextView);
//		change_dirButton = (Button) view.findViewById(R.id.change_dirButton);
		log_dirTextView  = (TextView) view.findViewById(R.id.log_dir_pathTextView);
		autorunCheckBox = (CheckBox) view.findViewById(R.id.autorunCheckBox);
		
		startButton.setOnClickListener(this);
//		change_dirButton.setOnClickListener(this);
		log_dirTextView.setOnClickListener(this);

		if(LogBatteryService.getServiceIsRun())
        {
        	Log.d("isRun","service is run");
			countIsRun = true;
			startButton.setText("stop");
			intervalEditText.setEnabled(false);
			commentEditText.setEnabled(false);
//			change_dirButton.setEnabled(false);
			log_dirTextView.setTextColor(getResources().getColor(R.color.geos_grey));
			loadConfig();
			intervalEditText.setText(String.valueOf(LogBatteryService.getQueryInterval()));
			commentEditText.setText(String.valueOf(LogBatteryService.getComment()));
			logNameTextView.setText(String.valueOf(LogBatteryService.getLogFileName()));
        }else{
        	loadConfig();
        }
//        Log.d("device info", Build.BRAND);
//        Log.d("device info", Build.MODEL);
//        Log.d("device info", Build.VERSION.RELEASE);
//        Log.d("device info", Build.PRODUCT);
//        Log.d("device info", Build.BOARD);
//        Log.d("device info", Build.DEVICE);
//        Log.d("device info", Build.DISPLAY);
//        Log.d("device info", Build.HARDWARE);
//        Log.d("device info", Build.ID);
//        Log.d("device info", Build.TYPE);
//        Log.d("device info", Build.CPU_ABI);
//        Log.d("device info", Build.CPU_ABI2);
//        Log.d("device info", Build.HOST);
//        Log.d("device info", Build.FINGERPRINT);
//        Log.d("device info", Build.BOOTLOADER);
		TextWatcher watcher = new TextWatcher() {
			@Override
			public void onTextChanged(CharSequence s, int start, int before, int count) {}
			@Override
			public void beforeTextChanged(CharSequence s, int start, int count,	int after) {}
			@Override
			public void afterTextChanged(Editable s) {
				saveConfig();
			}
		};
		commentEditText.addTextChangedListener(watcher);
		intervalEditText.addTextChangedListener(watcher);
		return view;
	}

	@Override
	public void onClick(View v) {
		// TODO Auto-generated method stub
		MainDrawerActivity fActivity = (MainDrawerActivity) getActivity();
		switch(v.getId()){
		case R.id.startButtonId:
/* ������� �� ������ start/stop */			
			if(countIsRun){
				Log.d("click","stop work");
				fActivity.stopService(new Intent(fActivity, LogBatteryService.class));
				countIsRun = false;
				intervalEditText.setEnabled(true);
				commentEditText.setEnabled(true);
//				change_dirButton.setEnabled(true);
				startButton.setText("start");
				log_dirTextView.setTextColor(Color.BLACK);
				
				SharedPreferences sPref = getActivity().getSharedPreferences(ConfigDefault.PREFERENCES_NAME, Activity.MODE_PRIVATE);
		    	Editor ed = sPref.edit();
		    	ed.putBoolean(ConfigKey.stop_by_power, false);
		    	ed.putString(ConfigKey.continue_log_name, "");
		    	ed.commit();
			}else{
				Log.d("click","start work");
				startButton.setText("stop");
//				change_dirButton.setEnabled(false);
				commentEditText.setEnabled(false);
				intervalEditText.setEnabled(false);
				log_dirTextView.setTextColor(getResources().getColor(R.color.geos_grey));
				int query_interval;

				String query_intervalString = intervalEditText.getText().toString();

				if(query_intervalString.equals("")){
					query_interval = 1;
					intervalEditText.setText("1");
				}else{
					Log.d("config frag", query_intervalString);
					query_interval = Integer.parseInt(query_intervalString);
					if(query_interval <= 0){
						query_interval = 1;
						intervalEditText.setText("1");
					}
				}
				
				saveConfig();
				Intent intent = new Intent(fActivity, LogBatteryService.class);
				intent.putExtra("log_directory", log_dirTextView.getText().toString());
				intent.putExtra("query_interval", query_interval);
				intent.putExtra("comment", commentEditText.getText().toString());
				fActivity.startService(intent);
				
				IntentFilter filter = new IntentFilter();
				filter.addAction("LogServiceStart");
				serviceBroadcastReceiver = new BroadcastReceiver() {
					@Override
					public void onReceive(Context context, Intent intent) {
						// TODO Auto-generated method stub
						String log_name = intent.getStringExtra("log_name");
						logNameTextView.setText(log_name);
					}
				};
				getActivity().registerReceiver(serviceBroadcastReceiver, filter);
				
				countIsRun = true;
			}
			break;
		case R.id.log_dir_pathTextView:
			/* ����� ���������� ��� �������� ����� */
			if(countIsRun){
				break;
			}
			Intent intent = new Intent(fActivity, FileExplorerActivity.class);
			intent.putExtra(SettingsConst.KEY_CURRENT_DIRECTORY, Environment.getExternalStorageDirectory().getAbsolutePath());
			intent.putExtra(SettingsConst.KEY_SHOW_DIRECTORIES, true);
//			intent.putExtra(SettingsConst.KEY_EXTENTION_ARRAY, new String[] {Extension.JPG, Extension.JPEG, Extension.BMP, Extension.JSON});
			intent.putExtra(SettingsConst.KEY_MODE_EXPLORER, MODE.DIRECTORIES.ordinal());
			Log.d("activity res", "on click button");
			startActivityForResult(intent, REQUEST_LOG_DIRECTORY);
			break;
		}
	}
	
	@Override
	public void onActivityResult(int requestCode, int resultCode, Intent intent) {
		// TODO Auto-generated method stub
		if(requestCode == REQUEST_LOG_DIRECTORY){
			/* ��������� ���������� ������ ����������. FileExplorerActivity */
			if(resultCode == Activity.RESULT_OK && intent != null){
				String log_dir = intent.getStringExtra(SettingsConst.KEY_RESULT_PATH);
				log_dirTextView.setText(log_dir);
				Log.d("activity res", log_dir);
			} else if(resultCode == Activity.RESULT_CANCELED){
//				do nothing
			}
		}
	}
	@Override
	public void onDestroy() {
		saveConfig();
		if(serviceBroadcastReceiver != null){
			getActivity().unregisterReceiver(serviceBroadcastReceiver);
			serviceBroadcastReceiver = null;
		}
		super.onDestroy();
	};
//	���������� �������� ����������(�������� ������, ���������� ��� �����, ...)
	private void saveConfig(){
		Log.d("save","save config");
		sPref = PreferenceManager.getDefaultSharedPreferences(getActivity());
		Editor ed = sPref.edit();
		int interval;
		String intervalString = intervalEditText.getText().toString();
		if(intervalString.equals("")){
			interval = ConfigDefault.DEFAULT_INTERVAL;
		}else{
			interval = Integer.parseInt(intervalString);
		}
		ed.putInt(ConfigKey.interval, interval);
		ed.putString(ConfigKey.log_dir, log_dirTextView.getText().toString());
		ed.putString(ConfigKey.comment, commentEditText.getText().toString());
		ed.putBoolean(ConfigKey.autorun, autorunCheckBox.isChecked());
		ed.commit();
	}
//	�������� �������� ����������
	private void loadConfig(){
		sPref = PreferenceManager.getDefaultSharedPreferences(getActivity());
		intervalEditText.setText(String.valueOf(sPref.getInt(ConfigKey.interval, ConfigDefault.DEFAULT_INTERVAL)));
		log_dirTextView.setText(sPref.getString(ConfigKey.log_dir, ConfigDefault.DEFAULT_LOG_DIR));
		Log.d("config", ConfigDefault.DEFAULT_LOG_DIR);
		Log.d("config", sPref.getString(ConfigKey.log_dir, ConfigDefault.DEFAULT_LOG_DIR));
		Log.d("config", Environment.getExternalStorageDirectory().getAbsolutePath());
		Log.d("config", "autorun: "+String.valueOf(sPref.getBoolean(ConfigKey.autorun, ConfigDefault.DEFAULT_AUTORUN)));
		commentEditText.setText(sPref.getString(ConfigKey.comment, ConfigDefault.DEFAULT_COMMENT));
		autorunCheckBox.setChecked(sPref.getBoolean(ConfigKey.autorun, ConfigDefault.DEFAULT_AUTORUN));
	}
}

package com.example.battery;

//#region Copyright
//Copyright ArtfulBits Inc. 2005 - 2011. All rights reserved.
//
//Use of this code is subject to the terms of our license.
//A copy of the current license can be obtained at any time by e-mailing
//info@artfulbits.com. Re-distribution in any form is strictly
//prohibited. Any infringement will be prosecuted under applicable laws.
//#endregion

import android.content.Context;
import android.util.Log;
import android.view.MotionEvent;

import com.artfulbits.aiCharts.ChartView;
import com.artfulbits.aiCharts.Base.ChartArea;
import com.artfulbits.aiCharts.Base.ChartAxis;
import com.artfulbits.aiCharts.Base.ChartAxisScale;
/*
 * view-�� ��� ����������� �������� � ���������� zoom-�
 * */
public class ChartViewAdv extends ChartView
{
	private double m_touchDistance1 = Double.NaN;
	
	private double m_touchDistance2 = Double.NaN;
	
	private double m_zoomFactor = 1;
	
	ChartAxis mSecAxis;
	
	private ChartArea m_zoomArea = null;
	
	private boolean m_zoomXAxis = false;
	
	private boolean m_zoomYAxis = false;
	
	/**
	 * @param context
	 * @param chartId
	 */
	public ChartViewAdv(Context context, int chartId)
	{
		super(context, chartId);
	}
	
	public void enableZooming(ChartArea area, boolean xAxis, boolean yAxis, ChartAxis secAxis)
	{
		m_zoomArea = area;
		
		m_zoomXAxis = xAxis;
		m_zoomYAxis = yAxis;
		
		mSecAxis = secAxis;
	}
	
	public void disableZooming()
	{
		m_zoomArea = null;
	}
	
	/*
	 * (non-Javadoc)
	 * 
	 * @see
	 * com.artfulbits.aiCharts.ChartView#onTouchEvent(android.view.MotionEvent)
	 */
	@Override
	public boolean onTouchEvent(MotionEvent event)
	{
		if(m_zoomArea != null && event.getPointerCount() > 1)
		{
			if(event.getAction() == MotionEvent.ACTION_MOVE)
			{
				double dx = event.getX(0) - event.getX(1);
				double dy = event.getY(0) - event.getY(1);
				
				// compute distance between pointers
				m_touchDistance2 = Math.sqrt(dx * dx + dy * dy);
//				Log.d("zoom", String.valueOf(m_touchDistance2));
				if(Double.isNaN(m_touchDistance1))
				{
					m_touchDistance1 = m_touchDistance2;
				}
				else
				{
					// compute current zoom factor
					double currentZoomFactor = m_zoomFactor * m_touchDistance1
							/ m_touchDistance2;
					zoomToFactor(currentZoomFactor);
				}
			}
			
			return true;
		}
		else if(!Double.isNaN(m_touchDistance1))
		{
			// save current zoom factor
			m_zoomFactor *= m_touchDistance1 / m_touchDistance2;
			
			// zoom factor can't be more that one
			m_zoomFactor = validateFactor(m_zoomFactor);
			
			// reset distances
			m_touchDistance1 = Double.NaN;
			m_touchDistance2 = Double.NaN;
			
			return true;
		}
		
		return super.onTouchEvent(event);
	}
	
	private double validateFactor(double factor)
	{
		factor = Math.min(factor, 1.0d);
		factor = Math.max(factor, 0.01d);
		
		return factor;
	}
	
	private void zoomToFactor(double scale)
	{
		scale = validateFactor(scale);
		
		if(m_zoomArea != null)
		{
			if(m_zoomXAxis)
				m_zoomArea.getDefaultXAxis().getScale().zoomToFactor(scale);
			
			if(m_zoomYAxis)
			{
				//m_zoomArea.getDefaultYAxis().getScale().zoomToFactor(scale);
				//mSecAxis.getScale().zoomToFactor(scale);
				
				for(ChartAxis yAxis : m_zoomArea.getAxes())
				{
					if(yAxis != m_zoomArea.getDefaultXAxis())
					{
						yAxis.getScale().zoomToFactor(scale);
					}
				}
			}
		}
	}
}


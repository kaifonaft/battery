package com.example.battery;

import java.io.File;
import java.io.FileFilter;
import java.util.HashSet;
import java.util.Iterator;
import java.util.Set;

public class FileExtensionsFilter implements FileFilter {
	private Set<String> exts = new HashSet<String>();
	private boolean isAddDirectory = false;

	/**
	 * @param extensions
	 *            a list of allowed extensions, without the dot, e.g.
	 *            <code>"xml","html","rss"</code>
	 */
	public FileExtensionsFilter(String... extensions) {
		for (String ext : extensions) {
			exts.add("." + ext.toLowerCase().trim());
		}
	}
	
	public void setDirectoryEnabled(boolean isAddDir) {
		isAddDirectory = isAddDir;
	}



	@Override
	public boolean accept(File file) {
		final Iterator<String> extList = exts.iterator();
		if (isAddDirectory && file.isDirectory()) {
			return true;
		}
		
		while (extList.hasNext()) {
			if (file.getAbsolutePath().toLowerCase().endsWith(extList.next())) {
				return true;
			}
		}
		return false;
	}

}

package com.example.battery;

import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import java.io.File;
import java.util.ArrayList;

import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Environment;
import android.preference.PreferenceManager;
import android.util.Log;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.ListView;
import android.widget.AdapterView.OnItemClickListener;
import java.util.Arrays;

public class ResultListFragment extends Fragment{
	/*
	 * �������� ��� ����������� ������ �����
	 * */
    ListView fileListView;
	@Override
	public View onCreateView(LayoutInflater inflater, ViewGroup container,
			Bundle savedInstanceState) {
		// TODO Auto-generated method stub
		View view = inflater.inflate(R.layout.result_list, container, false);
		fileListView = (ListView) view.findViewById(R.id.picker_fileListView);
    	SharedPreferences sPref = PreferenceManager.getDefaultSharedPreferences(getActivity());
    	
    	String rootDirName = sPref.getString(ConfigKey.log_dir, Environment.getExternalStorageDirectory().getAbsolutePath());
		File mfile = new File(rootDirName); //new File(Environment.getExternalStorageDirectory().getAbsolutePath());
    	File[] list=mfile.listFiles();
    	Arrays.sort(list);
    	String prefix = LogBatteryService.getLogFileNamePrefix(), suffix = LogBatteryService.getLogFileNameSuffix();
    	ArrayList<String> nameList = new ArrayList<String>();
    	for(int i=0; i<list.length; i++){
    		String name = list[i].getName();
	    	if(name.startsWith(prefix) && name.endsWith(suffix)) {
	    		Log.d("show res",list[i].getName());
	    		nameList.add(list[i].getName());
	    	}
    	}
    	ArrayAdapter<String> adapter = new ArrayAdapter<String>(getActivity(), android.R.layout.simple_list_item_1, nameList);
    	fileListView.setAdapter(adapter);
    	fileListView.setOnItemClickListener(new OnItemClickListener() {
			@Override
			public void onItemClick(AdapterView<?> arg0, View view, int position,
					long id) {
				// TODO Auto-generated method stub
				Log.d("list view","itemClick: position = "+position+", id = "+id);
				String logFileName = (String) fileListView.getItemAtPosition(position);
				Intent intent = new Intent(getActivity(), ShowResultActivity.class);
				intent.putExtra("logFileName", logFileName);
				getActivity().startActivity(intent);
			}
		});
//		return inflater.inflate(R.layout.log_picker, container);
    	return view;
	}
}



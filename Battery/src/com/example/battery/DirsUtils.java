package com.example.battery;

import java.io.File;
import java.io.FilenameFilter;
import java.io.InputStream;
import java.util.ArrayList;
import java.util.HashSet;
import java.util.Locale;

//import ru.sitis.geoscamera.prefs.AppPrefs;
import android.os.Environment;


public class DirsUtils {

//	/* *******************************************************
//	 * 			����������� ���������� � ������ � ������
//	 ******************************************************* */
	public static final String ROOT_DIR = "Battery";
	private static File logDir;
	
	public static boolean isExternalStorageMounted(){
	     String state = Environment.getExternalStorageState();                   
	     return state.equals(Environment.MEDIA_MOUNTED);         
	}	
	
	public static HashSet<String> getExternalMounts() {
	    final HashSet<String> out = new HashSet<String>();
	    String reg = "(?i).*vold.*(vfat|ntfs|exfat|fat32|ext3|ext4).*rw.*";
	    String s = "";
	    try {
	        final Process process = new ProcessBuilder().command("mount")
	                .redirectErrorStream(true).start();
	        process.waitFor();
	        final InputStream is = process.getInputStream();
	        final byte[] buffer = new byte[1024];
	        while (is.read(buffer) != -1) {
	            s = s + new String(buffer);
	        }
	        is.close();
	    } catch (final Exception e) {
	        e.printStackTrace();
	    }

	    // parse output
	    final String[] lines = s.split("\n");
	    for (String line : lines) {
	        if (!line.toLowerCase(Locale.US).contains("asec")) {
	            if (line.matches(reg)) {
	                String[] parts = line.split(" ");
	                for (String part : parts) {
	                    if (part.startsWith("/"))
	                        if (!part.toLowerCase(Locale.US).contains("vold"))
	                            out.add(part);
	                }
	            }
	        }
	    }
	    return out;
	}
	
	public static ArrayList<String> getStorages(){
		ArrayList<String> list = new ArrayList<String>();
		
		if(isExternalStorageMounted()){
			HashSet<String> hashset = getExternalMounts();
			list.addAll(new ArrayList<String>(hashset));
		}
		list.add(Environment.getExternalStorageDirectory().getAbsolutePath());
		
		return list;
	}
	
	
	public static String[] getDirectoryList(File path){
		
		if(path.exists()){

			FilenameFilter nameFilter = new FilenameFilter() {	    
	    		public boolean accept(File dir, String filename) {	
	    			File sel = new File(dir, filename);
	    			return sel.isDirectory();						                	
	    		}
		    };
	    		    	
		    if (path.list(nameFilter) != null)
	    		return path.list(nameFilter);
	    		    	
		}
		return new String[0];
	}
	
	
	public static File getBatteryDir() {
		//if(!isExternalStorageMounted());
		if(logDir == null){		
			logDir = new File(Environment.getExternalStorageDirectory(), ROOT_DIR);
			if(!logDir.exists()){
				logDir.mkdir();
			}
		}
		return logDir;
	}

}

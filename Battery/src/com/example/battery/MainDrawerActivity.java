package com.example.battery;

import android.content.Intent;
import android.os.Bundle;
import android.support.v4.app.FragmentActivity;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.NavUtils;
import android.util.Log;
import android.view.MenuItem;

public class MainDrawerActivity extends DrawerActivity{

	@Override
	protected void selectItem(int tab) {
		// TODO Auto-generated method stub
		FragmentManager fm = getSupportFragmentManager();
		switch(tab){
		case CONFIG:
			ConfigFragment frag1 = new ConfigFragment();
			fm.beginTransaction().replace(R.id.content_frame, frag1, "frag1").commit();
			break;
		case RESULT:
			ResultListFragment frag2 = new ResultListFragment();
			fm.beginTransaction().replace(R.id.content_frame, frag2, "frag2").commit();
			break;
		case ABOUT:
			AboutFragment frag3 = new AboutFragment();
			fm.beginTransaction().replace(R.id.content_frame, frag3, "frag3").commit();
			break;
		}
		setTitle(mDrawerTitles[tab]);
	}
}

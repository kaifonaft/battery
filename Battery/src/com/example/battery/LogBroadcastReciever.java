package com.example.battery;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.preference.PreferenceManager;
import android.util.Log;

public class LogBroadcastReciever extends BroadcastReceiver {
/*
 * ��������� ����������� �������*/
	@Override
	public void onReceive(Context context, Intent intent) {
		// TODO Auto-generated method stub
		SharedPreferences sPref = PreferenceManager.getDefaultSharedPreferences(context);
//		SharedPreferences sPref = getSharedPreferences("logService", MODE_PRIVATE);
//		Log.d("config", DEFAULT_LOG_DIR);
		boolean autorun;
		autorun = sPref.getBoolean(ConfigKey.autorun, ConfigDefault.DEFAULT_AUTORUN);
		if(autorun){
			String comment, log_dir;
			int interval;
			interval = sPref.getInt(ConfigKey.interval, ConfigDefault.DEFAULT_INTERVAL);
			comment = sPref.getString(ConfigKey.comment, ConfigDefault.DEFAULT_COMMENT);
			log_dir = sPref.getString(ConfigKey.log_dir, ConfigDefault.DEFAULT_LOG_DIR);

			Intent serviceIntent = new Intent(context, LogBatteryService.class);
			serviceIntent.putExtra("log_directory", log_dir);
			serviceIntent.putExtra("query_interval", interval);
			serviceIntent.putExtra("comment", comment);
			
			Log.d("broadcast", "onReceive");
			context.startService(serviceIntent);
		}
	}
}

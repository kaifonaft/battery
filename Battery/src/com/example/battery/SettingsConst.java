package com.example.battery;

public class SettingsConst {
	
	/* *********************
	 * 		KEYS
	 ************************ */
	
	public static final int KEY_AUTHOR = 4;

	public static final String APP_ROOT_DIRECTORY = "GeoS";
	public static final String KEY_CURRENT_DIRECTORY = "current_directory";
	
	public static final String KEY_SHOW_DIRECTORIES = "key_only_directory";
	public static final String KEY_EXTENTION_ARRAY = "key_extention_array";
	
	public static final String KEY_MODE_EXPLORER = "key_mode_explorer";
	
	public static final String KEY_RESULT_PATH = "key_result_path";

}
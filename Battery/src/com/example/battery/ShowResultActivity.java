package com.example.battery;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileReader;
import java.util.Calendar;
import java.util.Date;
import java.util.Iterator;
import java.text.SimpleDateFormat;

import android.app.Activity;
import android.app.AlertDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.SharedPreferences;
import android.content.res.Configuration;
import android.graphics.Color;
import android.os.Bundle;
import android.os.Environment;
import android.preference.PreferenceManager;
import android.util.Log;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;
import android.widget.RelativeLayout;
import android.widget.TextView;

import org.json.JSONArray;
import org.json.JSONObject;

import com.artfulbits.aiCharts.ChartGestureListener;
import com.artfulbits.aiCharts.ChartView;
import com.artfulbits.aiCharts.Base.ChartAxis;
import com.artfulbits.aiCharts.Base.ChartAxisScale;
import com.artfulbits.aiCharts.Base.ChartSeries;
import com.artfulbits.aiCharts.Types.ChartLineType;
import com.artfulbits.aiCharts.Types.ChartLineType.BreakMode;
import com.artfulbits.aiCharts.Types.ChartTypes;
import com.example.battery.ChartViewAdv;
import com.example.battery.R;

public class ShowResultActivity extends Activity{
	/* view-�� ����������� �������� */
	Button backToSettingsButton;
	private final int voltMin = 3500,
		voltMax = 4500,
		tempMin = 0,
		tempMax = 500,
		batMin = 0,
		batMax = 100;
	private long timeMin, timeMax;
	private BatteryState[] saveBatteryState;
	private int query_interval;
	private int batteryColor,
			voltageColor,
			temperatureColor,
			fonColor = Color.WHITE,
			scaleColor;
	TextView volt_rangeTextView,
		temp_rangeTextView,
		bat_rangeTextView,
		date_rangeTextView,
		deviceTextView,
		commentTextView;
	private RelativeLayout canvas;
	private String save_deviceString, save_commentString, save_date_range,
		save_bat_range, save_volt_range, save_temp_range;

	@Override
	public void onConfigurationChanged(android.content.res.Configuration newConfig) {
		Log.d("show res", "orientation");
		if(newConfig.orientation == Configuration.ORIENTATION_LANDSCAPE){
			Log.d("show res", "landscape");
	        setContentView(R.layout.show_result_landscape);
		}else{
			Log.d("show res", "portrait");
	        setContentView(R.layout.show_result);
		}
		initUI();
		final ChartViewAdv chartView = new ChartViewAdv(this, R.xml.chart);
		configChartView(chartView);
		canvas.addView(chartView);
		loadTextView();
		super.onConfigurationChanged(newConfig);
	};
//	������������� View-� 
	private void initUI(){
		if(getResources().getConfiguration().orientation == Configuration.ORIENTATION_LANDSCAPE){
			Log.d("show res", "landscape");
	        setContentView(R.layout.show_result_landscape);
		}else{
			Log.d("show res", "portrait");
	        setContentView(R.layout.show_result);
		}
        canvas = (RelativeLayout) findViewById(R.id.canvas);
        volt_rangeTextView = (TextView) findViewById(R.id.voltage_rangeTextView);
        temp_rangeTextView = (TextView) findViewById(R.id.temperature_rangeTextView);
        bat_rangeTextView = (TextView) findViewById(R.id.battery_rangeTextView);
        date_rangeTextView = (TextView) findViewById(R.id.date_rangeTextView);
        deviceTextView = (TextView) findViewById(R.id.deviceTextView);
        commentTextView = (TextView) findViewById(R.id.commentTextView);
	}
//	������������ ����� � TextView
	private void loadTextView(){
        volt_rangeTextView.setText(save_volt_range);
        bat_rangeTextView.setText(save_bat_range);
        temp_rangeTextView.setText(save_temp_range);
        date_rangeTextView.setText(save_date_range);
        deviceTextView.setText(save_deviceString);
        commentTextView.setText(save_commentString);
	}
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		Log.d("show res", "on create");
        getActionBar().setDisplayHomeAsUpEnabled(true);
        getActionBar().setHomeButtonEnabled(true);

		// TODO Auto-generated method stub
		super.onCreate(savedInstanceState);
		batteryColor = getResources().getColor(R.color.battery);
		voltageColor = getResources().getColor(R.color.voltage);
		temperatureColor = getResources().getColor(R.color.temperature);
		scaleColor = getResources().getColor(R.color.scale);
		
		initUI();
        save_bat_range = String.valueOf(batMin) + " - " + String.valueOf(batMax) + " %";
        save_temp_range = String.valueOf(tempMin/10.0) + " - " + String.valueOf(tempMax/10.0) + " \u00B0"+"C";
        save_volt_range = String.valueOf(voltMin/1000.0) + " - " + String.valueOf(voltMax/1000.0)+" v";

        boolean errors = false;
	    try {
	    	Intent intent = getIntent();
	    	String logFileName = intent.getStringExtra("logFileName");
	    	setTitle(logFileName);
	    	
	    	SharedPreferences sPref = PreferenceManager.getDefaultSharedPreferences(this);
	    	String dirName = sPref.getString(ConfigKey.log_dir, Environment.getExternalStorageState());
	    	Log.d("show res", dirName);
	    	File f = new File(dirName, logFileName);
	    	BufferedReader fr = new BufferedReader(new FileReader(f));

	    	String res="";
	    	char[] buf = new char[1024*1024];
	    	int lRead=0;
	    	while((lRead = fr.read(buf)) != -1){
	    		res = res + new String(buf,0,lRead);
	    	}
	    	
	    	fr.close();
	    	
	    	res = "["+res+"]";
	    	JSONArray json = new JSONArray(res);
	    	int len = json.length();
	    	Log.d("length", String.valueOf(len));
	    	JSONObject obj,comment, device_info, query_intervalJSON;
    		device_info = json.getJSONObject(0);
    		query_intervalJSON = json.getJSONObject(1);
    		comment = json.getJSONObject(2);
    		query_interval = query_intervalJSON.getInt("query_interval");
    		device_info.keys();
    		Iterator it = device_info.keys();
    		while(it.hasNext()){
    			String s = (String) it.next();
    		}
	        save_deviceString = device_info.getString("BRAND") + " " + device_info.getString("MODEL");
	        save_commentString = comment.getString("comment"); 
	        
	    	obj = json.getJSONObject(3);
			SimpleDateFormat formatter = new SimpleDateFormat("yyyy.MM.dd hh:mm:ss");
			String strTime;
			long time;
			int BSi = 0;
	    	saveBatteryState = new BatteryState[len-3];
	    	for(int i=3; i<len; i++){
	    		obj = json.getJSONObject(i);
	    		saveBatteryState[BSi] = new BatteryState();
	    		saveBatteryState[BSi].battery_level = obj.getInt("bl");
	    		saveBatteryState[BSi].temp = obj.getInt("t");
	    		saveBatteryState[BSi].volt = obj.getInt("v");
	    		strTime = obj.getString("time");
	    		time = formatter.parse(strTime).getTime();
	    		saveBatteryState[BSi].time = time;
	    		BSi++;
	    	}
	    	timeMin = saveBatteryState[0].time;
	    	timeMax = saveBatteryState[BSi-1].time;
	        SimpleDateFormat screenFormat = new SimpleDateFormat("yyyy.MM.dd hh:mm");
	        save_date_range = screenFormat.format(new Date(timeMin)) + " - " + screenFormat.format(new Date(timeMax));
	        loadTextView();
	    } catch (Exception e) {
	        e.printStackTrace();
	        errors = true;
	    }
	    if(!errors){
			final ChartViewAdv chartView = new ChartViewAdv(this, R.xml.chart);
			configChartView(chartView);
			canvas.addView(chartView);
	    }else{
	    	AlertDialog.Builder builder = new AlertDialog.Builder(this);
	    	builder.setMessage("Error reading file!")
	    	       .setCancelable(false)
	    	       .setPositiveButton("OK", new DialogInterface.OnClickListener() {
	    	           public void onClick(DialogInterface dialog, int id) {
	    	        	   Log.d("show result", "OK");
	    	        	   finish();
	    	           }
	    	       });
	    	AlertDialog alert = builder.create();
	    	alert.show();
	    }
	}
//	��������� �����(������ �����) ��� ������� ������ �������
	private void configBattery(ChartSeries series, ChartView chartView){
		final int interval_eps = 1000;
		double BREAK_DELTA = query_interval * 1000 + interval_eps;
		series.setAttribute(ChartLineType.BREAK_MODE, BreakMode.Auto);
		series.setAttribute(ChartLineType.BREAK_DELTA, BREAK_DELTA);

		series.setLineWidth(5);
		series.setXAxis(chartView.getAreas().get(0).getDefaultXAxis());
		series.setArea(chartView.getAreas().get(0).getName());
		series.setBackColor(batteryColor);
		series.setLabel("battery");

		ChartAxis chartAxis = new ChartAxis(ChartAxis.Position.Left);

		chartView.getAreas().get(0).getAxes().add(chartAxis);
		series.setYAxis(chartAxis);
		((ChartViewAdv) chartView).enableZooming(chartView.getAreas().get(0), true, false, chartAxis);
		
		ChartAxisScale chartAxisScale = chartAxis.getScale();
		chartAxisScale.setMinimum(0d);
		chartAxisScale.setMaximum(100d);
		chartAxisScale.setInterval(20d);
		chartAxis.getLabelPaint().setColor(Color.BLACK);
		chartAxis.getLinePaint().setColor(Color.BLACK);
		chartAxis.getTitlePaint().setColor(Color.BLACK);
	}
//	��������� ����� ��� ������� ����������
	private void configVoltage(ChartSeries series, ChartView chartView){
		final int interval_eps = 1000;
		double BREAK_DELTA = query_interval * 1000 + interval_eps;
		series.setAttribute(ChartLineType.BREAK_MODE, BreakMode.Auto);
		series.setAttribute(ChartLineType.BREAK_DELTA, BREAK_DELTA);

		series.setLineWidth(5);
		series.setXAxis(chartView.getAreas().get(0).getDefaultXAxis());
		series.setArea(chartView.getAreas().get(0).getName());
		series.setBackColor(voltageColor);
		series.setLabel("voltage");
		ChartAxis chartAxis = new ChartAxis(ChartAxis.Position.Left);
		chartView.getAreas().get(0).getAxes().add(chartAxis);
		series.setYAxis(chartAxis);
		
		chartAxis.setTitle("voltage");
		chartAxis.getTitlePaint().setColor(Color.BLACK);
		chartAxis.getTitlePaint().setTextSize(16f);

		ChartAxisScale chartAxisScale = chartAxis.getScale();
		chartAxisScale.setMinimum(3.5);
		chartAxisScale.setMaximum(4.5);
		chartAxisScale.setInterval(0.1);
		chartAxis.getLabelPaint().setColor(Color.BLACK);
		chartAxis.getLinePaint().setColor(Color.BLACK);
		chartAxis.getTitlePaint().setColor(Color.BLACK);
	}
//	��������� ����� ��� ������� �����������
	private void configTemperature(ChartSeries series, ChartView chartView){
		final int interval_eps = 1000;
		double BREAK_DELTA = query_interval * 1000 + interval_eps;
		series.setAttribute(ChartLineType.BREAK_MODE, BreakMode.Auto);
		series.setAttribute(ChartLineType.BREAK_DELTA, BREAK_DELTA);

		series.setLineWidth(5);
		series.setXAxis(chartView.getAreas().get(0).getDefaultXAxis());
		series.setArea(chartView.getAreas().get(0).getName());
		series.setBackColor(temperatureColor);
		series.setLabel("voltage");
		ChartAxis chartAxis = new ChartAxis(ChartAxis.Position.Right);
		chartView.getAreas().get(0).getAxes().add(chartAxis);
		series.setYAxis(chartAxis);
		
		chartAxis.setTitle("temperature");
		chartAxis.getTitlePaint().setColor(Color.BLACK);
		chartAxis.getTitlePaint().setTextSize(16f);

		ChartAxisScale chartAxisScale = chartAxis.getScale();
		chartAxisScale.setMinimum(0.0);
		chartAxisScale.setMaximum(50.0);
		chartAxisScale.setInterval(5.0);
		chartAxis.getLabelPaint().setColor(Color.BLACK);
		chartAxis.getLinePaint().setColor(Color.BLACK);
		chartAxis.getTitlePaint().setColor(Color.BLACK);
	}
	
//	��������� ����� ��� ����������� ��������
	private void configChartView(ChartViewAdv chartView){
		
		chartView.getAreas().get(0).getAxes().remove(chartView.getAreas().get(0).getDefaultYAxis());
		if(android.os.Build.VERSION.SDK_INT >= android.os.Build.VERSION_CODES.HONEYCOMB) {
            chartView.setLayerType(View.LAYER_TYPE_SOFTWARE, null);
        }
		
		ChartGestureListener gest = new ChartGestureListener(chartView, ChartGestureListener.HORIZONTAL_PANNING_FLAG);
		chartView.setGestureDetector(gest);
		
		chartView.getAreas().get(0).getDefaultXAxis().setScrollBarEnabled(true);
		
		ChartSeries batterySeries = new ChartSeries(ChartTypes.Line),
			temperatureSeries = new ChartSeries(ChartTypes.Line),
			voltageSeries = new ChartSeries(ChartTypes.Line);
		
		configBattery(batterySeries, chartView);
		configVoltage(voltageSeries, chartView);
		configTemperature(temperatureSeries, chartView);

		chartView.getSeries().add(batterySeries);
		chartView.getSeries().add(voltageSeries);
		chartView.getSeries().add(temperatureSeries);

		chartView.getAreas().get(0).getDefaultXAxis().getTitlePaint().setColor(Color.BLACK);
		chartView.getAreas().get(0).getDefaultXAxis().getLinePaint().setColor(Color.BLACK);
		chartView.getAreas().get(0).getDefaultXAxis().getLabelPaint().setColor(Color.BLACK);
		chartView.getAreas().get(0).getDefaultXAxis().setValueType(ChartAxis.ValueType.Date);



		SimpleDateFormat formatter = new SimpleDateFormat("yyyy.MM.dd hh:mm:ss");
		
		BatteryState cur;
		Calendar c = Calendar.getInstance();
		Log.d("count state", String.valueOf(saveBatteryState.length));
		Log.d("show res", String.valueOf(saveBatteryState.length));
		
		for(int i=0; i<saveBatteryState.length; i++){
			cur = saveBatteryState[i];
			c.setTimeInMillis(cur.time);

			batterySeries.getPoints().addDate(c.getTime(), cur.battery_level);
			voltageSeries.getPoints().addDate(c.getTime(), cur.volt/1000.0);
			temperatureSeries.getPoints().addDate(c.getTime(), cur.temp/10.0);
		}
	}
	@Override
	protected void onDestroy() {
		// TODO Auto-generated method stub
		Log.d("show res", "on destroy");
		super.onDestroy();
	}
	@Override
	public boolean onOptionsItemSelected(MenuItem item) {
	    switch (item.getItemId()) {
	    // Respond to the action bar's Up/Home button
	    case android.R.id.home:
	    	this.finish();
	    	return true;
	    default:
	    	return super.onOptionsItemSelected(item);
	    }
	}

}
